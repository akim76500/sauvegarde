import pymysql.cursors
import xlrd

# Connection bdd
connection = pymysql.connect(host='127.0.0.1',
                             port=8889,
                             user='root',
                             password='root',
                             db='base_product',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

# Declaration des constantes
book = xlrd.open_workbook("product.xls")
sheet = book.sheet_by_name("Feuille1")
i = 1000
skiped = 0
err_obj = []
L = 0
separator = "-------------------------------------------------"


try:
    with connection.cursor() as cursor:
        # Creation d'une nouvelle ligne
        query = """INSERT INTO produit (pr_refour, pr_desi, pr_codebarre,pr_pack, pr_prac, pr_prix, pr_cd_pr) VALUES( % s, % s, % s, % s, % s, % s, % s)"""
        # on parcour le fichier puis on test si chaque ligne est vide ou non
        for r in range(7, sheet.nrows):
            isok = True
            if sheet.cell(r,0).value != '' :
                pr_refour = sheet.cell(r,0).value
            else:
                err_obj.append("Impossible d'ajouter la ligne : "+repr(i)+" Le champ référence est vide")
                isok = False
            if repr(sheet.cell(r,2).value) != '' and repr(sheet.cell(r,3).value) != '':
                pr_desi = repr(sheet.cell(r,2).value + " " +  repr(sheet.cell(r,3).value))
            else:
                err_obj.append("Impossible d'ajouter la ligne : " + repr(i) + " Le champ designation est vide")
                isok = False
            if sheet.cell(r,4).value != '':
                 pr_pack = sheet.cell(r,4).value
            else :
                err_obj.append("Impossible d'ajouter la ligne : " + repr(i) + " Le champ pack est vide")
                isok = False
            if sheet.cell(r,7).value != '':
                pr_codebarre = sheet.cell(r,7).value
            else:
                err_obj.append("Impossible d'ajouter la ligne : " + repr(i) + " Le champ code barre est vide")

                isok = False
            if sheet.cell(r,5).value != '':
                pr_prac = sheet.cell(r,5).value
            else:
                err_obj.append("Impossible d'ajouter la ligne : " + repr(i) + " Le champ prac est vide")
                isok = False
            if sheet.cell(r,6).value:
                pr_prix = sheet.cell(r,6).value
            else:
                #Si isok n'est pas a true on n'ajoute pas la ligne car elle comprend une ligne vide
                isok = False
            if isok != False :
                pr_cd_pr = i
                i = i + 1
                values = (pr_refour, pr_desi, pr_codebarre,pr_pack, pr_prac, pr_prix, pr_cd_pr)
                print("Insertion de la ligne  =>",pr_refour, pr_desi, pr_codebarre,pr_pack, pr_prac, pr_prix, pr_cd_pr)
                cursor.execute(query, values)
                connection.commit()
            else:
                skiped = skiped + 1
            L = L + 1
finally:
    connection.close()
#Ajout des logs d'information dans err_obj
err_obj.append("Ligne non ajoutée car vide => : " + repr(skiped))
err_obj.append(separator)

#Creation de l'objet du fichier en mode écritur , puis on parcour le tableau d'erreur pour insertion de chaque ligne
file_object  = open("err.log", "w")
for i in err_obj:
    file_object.write(i+'\n')
file_object.close()

# Assign values from each row

# Execute sql Query

# Close the cursor


# Commit the transaction
